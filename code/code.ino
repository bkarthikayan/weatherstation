
#include <Arduino.h>
#include <Wire.h>
#include "Adafruit_SHT31.h"

// https://github.com/neosarchizo/ESP32_SHT31

#define I2C_SDA 18
#define I2C_SCL 19

uint64_t chipid;

void setup() {
  Serial.begin(115200);
  Wire.begin (I2C_SDA,I2C_SCL);
  Adafruit_SHT31 sht31 = Adafruit_SHT31(&Wire);

  Serial.println("SHT31 test");
  if (! sht31.begin(0x44)) {   // Set to 0x45 for alternate i2c addr
    Serial.println("Couldn't find SHT31");
    while (1) delay(1);
  }
}

void loop() {
  chipid = ESP.getEfuseMac(); //The chip ID is essentially its MAC address(length: 6 bytes).
  Serial.printf("ESP32 Chip ID = %04X", (uint16_t)(chipid >> 32)); //print High 2 bytes
  Serial.printf("%08X\n", (uint32_t)chipid); //print Low 4bytes.

  delay(3000);

}
